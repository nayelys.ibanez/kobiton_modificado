import io.restassured.path.json.JsonPath;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public abstract class SetUpDevice {




    /*-------Parametrizar-------*/
    private static String appVersion = "kobiton-store:92625";
    private static String userName = "jgrajales";
    private static String apiKey = "913d8398-cb6f-4a8c-b90f-ca710dd732e2";
    private static String platformVersion = "10";
    private static String platformName = "Android";

    /*-----------------------------------------------------------------------------------------*/

    public static JsonPath responseJson;
    public static String deviceUdId;
    public static String deviceName;


    /* Api Rest Request a Kobiton para obtener dispositivos en línea --- Se trabaja a partir de los siguientes modelos: https://api.kobiton.com/docs/#get-device-information */

    public static void getOnlineDevices() throws IOException {


        URL obj = new URL("https://api.kobiton.com/v1/devices?isOnline=true&isBooked=false&platformName="+platformName);
        String authString = userName + ":" + apiKey;
        byte[] encodedString = Base64.encodeBase64(authString.getBytes());
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Authorization", "Basic " + new String(encodedString));

        int responseCode = connection.getResponseCode();
        System.out.println("Código de respuesta http request getOnlineDevices a Kobiton: " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;

        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        String responseStr = (response.toString());
        responseJson = new JsonPath(responseStr);

    }

    //Filtro por las características deseadas del dispositivo a ejecutar, el primero que las cumpla retorna el Udid

    public static void getEspecificDevice() throws IOException, InterruptedException {

        getOnlineDevices();

        int count = responseJson.getInt("cloudDevices.size()");

        // String cloudDevices = responseJson.getString("cloudDevices");   Acá se devuelvería el response completo.
        //Esto se pudiese cambiar por un While.. me parece que sería mejor
        for (int i = 0; i < count; i++) {
            int Height = responseJson.get("cloudDevices[" + i + "].resolution.height");
            int Width = responseJson.get("cloudDevices[" + i + "].resolution.width");
            String devicePlatformVersion = responseJson.get("cloudDevices[" + i + "].platformVersion").toString();
            //System.out.println("Dispositivo " + i + ", Alto = " + Height + " Ancho = " + Width + " Versión de " + platformName + ": " + devicePlatformVersion);


            if ((evaluateResolution(Height, 2220, 2400)) && (evaluateResolution(Width, 1080, 1080)) && (devicePlatformVersion.equals(platformVersion))) {
                deviceUdId = responseJson.get("cloudDevices[" + i + "].udid").toString();
                deviceName = responseJson.get("cloudDevices[" + i + "].deviceName").toString();
                System.out.println("Ejecutando dispositivo: " + deviceName + " - UdId: " + deviceUdId);
                break;
            }
            if (i == count-1){
                System.out.println("Momentaneamente no hay dispositivos disponibles con la característica deseada, se volverá a intentar en 5 segundos.");
                Thread.sleep(5000);
                getEspecificDevice();
            }
        }
    }

    private static boolean evaluateResolution(int resolution, int Min, int Max) {
        return (Min <= resolution && resolution <= Max);
    }


    public static URL kobitonServerUrl() throws MalformedURLException
    {
         return new URL("https://jgrajales:913d8398-cb6f-4a8c-b90f-ca710dd732e2@api.kobiton.com/wd/hub");
    }


    public static DesiredCapabilities setCapabilities() throws IOException {

        FileInputStream fis= new FileInputStream(System.getProperty("user.dir")+"C:\\Users\\Nayelys.Ibanez\\Desktop\\ProyectoCrediCoop\\demokobiton\\src\\main\\java");
        Properties prop = new Properties();
        prop.load(fis);

        /*¿Cúales son las capabilities necesarias para esta ejecución?*/

        DesiredCapabilities capabilities = new DesiredCapabilities();

        String plataform= (String) prop.get("plataform");
        int group= (int) prop.get("group");


        capabilities.setCapability("sessionName", "Automation test session");
        capabilities.setCapability("sessionDescription", "");
        capabilities.setCapability("deviceOrientation", "portrait");
        capabilities.setCapability("captureScreenshots", true);
        //La App Se cambia cada vez que venga un paquete nuevo. Por ejemplo en archivo config. De momento manual
        capabilities.setCapability("app",appVersion);
        capabilities.setCapability("udid", deviceUdId);
        capabilities.setCapability("groupId", group); // Group: Manual
        capabilities.setCapability("deviceGroup", "KOBITON");
        capabilities.setCapability("platformVersion", "9");
        capabilities.setCapability("platformName", plataform);
        //capabilities.setCapability("appPackage", "coop.bancocredicoop.bancamobile");
        //capabilities.setCapability("appActivity", "coop.bancocredicoop.bancamobile.MainActivity");
        capabilities.setCapability("allowMobileShell", true);

        return capabilities;
    }

}

