import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class Home extends BasePage{

    public Home(AndroidDriver _driver) {
        super(_driver);
    }


    @AndroidFindBy(xpath="//*[@text='Menú lateral']")
    public WebElement menuLateral;

    @AndroidFindBy(xpath = "/html/body/ion-nav-view/ion-side-menus/ion-side-menu/ion-footer-bar/a")
   public  WebElement botonSalir;

    @AndroidFindBy(xpath = "/html/body/div[4]/div/div[3]/button[2]")
    public  WebElement botonConfirma;



    //MobileElement salir = (MobileElement) _driver.findElementByAccessibilityId("Salir"); //No se puede guardar el By a parte con este tipo de búsqueda por la estructura del mismo..


    //Incluir logOut en BasePage? De modo que peuda ser llamado dese cualquier ubicación de la app. Hay que confirmar que el identificador siga siendo el mismo independientmente de la pantalla. 
    public void logOut(){
        setContextNative();
        menuLateral.click();

        setContextWeb();
        botonSalir.click();
        botonConfirma.click();
    //    _driver.findElementByXPath("/html/body/ion-nav-view/ion-side-menus/ion-side-menu/ion-footer-bar/a").click(); //Botón salir
      //  _driver.findElementByXPath("/html/body/div[4]/div/div[3]/button[2]").click();   // botón si de confirmación
    }

}
